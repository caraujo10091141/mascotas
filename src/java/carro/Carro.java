/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package carro;

import entidad.Mascota;
import java.util.*;

/**
 *
 * @author Cesar Araujo
 */
public class Carro {

    //Lista de items en el carrito
    List<CarroItem> items;
    int numeroDeItems;
    double total;

    public Carro() {
        items = new ArrayList<CarroItem>();
        numeroDeItems = 0;
        total = 0;
    }

    /**
     * Adds a <code>CarroItem</code> to the <code>Carro</code>'s
     * <code>items</code> list. If item of the specified <code>product</code>
     * already exists in shopping carro list, the quantity of that item is
     * incremented.
     *
     * @param libro the <code>Libro</code> that defines the type of shopping carro item
     * @see CarroItem
     */
    public synchronized void agregaItem(Mascota mascota) {

        boolean newItem = true;

        //ciclo para recorrer todo el carrito
        for (CarroItem scItem : items) {

            //Si el producto ya esta en el carrito aumenta la cantidad pedida
            if (scItem.getMascota().getId() == mascota.getId()) {

                newItem = false;
                scItem.incrementaCantidad();
            }
        }

        //si es nu nuevo item a agregar al carrito lo inicializa
        if (newItem) {
            CarroItem scItem = new CarroItem(mascota);
            items.add(scItem);
        }
    }

    
    
    public synchronized void actualiza(Mascota mascota, String cantidad) {

        short cant = -1;

        //Convierte la cadena a short
        cant = Short.parseShort(cantidad);

        if (cant >= 0) {

            CarroItem item = null;

            for (CarroItem scItem : items) {

                if (scItem.getMascota().getId() == mascota.getId()) {

                    if (cant != 0) {
                        // Actualiza la cantidad de la mascota
                        scItem.setCantidad(cant);
                    } else {
                        // Si pone la cantidad en 0, eliminara el producto del carrito
                        item = scItem;
                        break;
                    }
                }
            }

            if (item != null) {
                // Elimina el producto en caso de poner 0
                items.remove(item);
            }
        }
    }

    
    //Obtiene la lista de productos en el carro
    public synchronized List<CarroItem> getItems() {

        return items;
    }

    
    //Obtiene el numero de items en el carro
    public synchronized int getNumeroDeItems() {

        numeroDeItems = 0;

        for (CarroItem scItem : items) {

            numeroDeItems += scItem.getCantidad();
        }

        return numeroDeItems;
    }

    //Obtiene el subtotal de cada articulo
    public synchronized double getSubtotal() {

        double subtotal = 0;

        for (CarroItem scItem : items) {

            Mascota product = (Mascota) scItem.getMascota();
            subtotal += (scItem.getCantidad() * product.getPrecio().doubleValue());
        }

        return subtotal;
    }

    //Calcula el total con el cargoEnvio
    public synchronized void calculateTotal(String surcharge) {

        double temp = 0;

        // cast surcharge as double
        double s = Double.parseDouble(surcharge);

        temp = this.getSubtotal();
        temp += s;

        total = temp;
    }

    
    public synchronized double getTotal() {

        return total;
    }

    
    //Limpia el carro
    public synchronized void clear() {
        items.clear();
        numeroDeItems = 0;
        total = 0;
    }

}