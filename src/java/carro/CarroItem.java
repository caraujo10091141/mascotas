/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package carro;

import entidad.Mascota;

/**
 *
 * @author Cesar Araujo
 */
public class CarroItem {

    Mascota mascota;
    short cantidad;

    public CarroItem(Mascota mascota) {
        this.mascota = mascota;
        cantidad = 1;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short quantity) {
        this.cantidad = quantity;
    }

    public void incrementaCantidad() {
        cantidad++;
    }

    public void reduceCantidad() {
        cantidad--;
    }

    public double getTotal() {
        double amount = 0;
        amount = (this.getCantidad() * mascota.getPrecio().doubleValue());
        return amount;
    }

}