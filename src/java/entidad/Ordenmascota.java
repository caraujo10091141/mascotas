/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Araujo
 */
@Entity
@Table(name = "ordenmascota")
@NamedQueries({
    @NamedQuery(name = "Ordenmascota.findAll", query = "SELECT o FROM Ordenmascota o"),
     @NamedQuery(name = "Ordenmascota.findByOrdenId", query = "SELECT o FROM Ordenmascota o WHERE o.ordenmascotaPK.ordenId = :ordenId")})
public class Ordenmascota implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrdenmascotaPK ordenmascotaPK;
    @Column(name = "cantidad")
    private Short cantidad;
    @JoinColumn(name = "orden_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Orden orden;
    @JoinColumn(name = "mascota_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Mascota mascota;

    public Ordenmascota() {
    }

    public Ordenmascota(OrdenmascotaPK ordenmascotaPK) {
        this.ordenmascotaPK = ordenmascotaPK;
    }

    public Ordenmascota(int ordenId, int mascotaId) {
        this.ordenmascotaPK = new OrdenmascotaPK(ordenId, mascotaId);
    }

    public OrdenmascotaPK getOrdenmascotaPK() {
        return ordenmascotaPK;
    }

    public void setOrdenmascotaPK(OrdenmascotaPK ordenmascotaPK) {
        this.ordenmascotaPK = ordenmascotaPK;
    }

    public Short getCantidad() {
        return cantidad;
    }

    public void setCantidad(Short cantidad) {
        this.cantidad = cantidad;
    }

    public Orden getOrden() {
        return orden;
    }

    public void setOrden(Orden orden) {
        this.orden = orden;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ordenmascotaPK != null ? ordenmascotaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ordenmascota)) {
            return false;
        }
        Ordenmascota other = (Ordenmascota) object;
        if ((this.ordenmascotaPK == null && other.ordenmascotaPK != null) || (this.ordenmascotaPK != null && !this.ordenmascotaPK.equals(other.ordenmascotaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Ordenmascota[ ordenmascotaPK=" + ordenmascotaPK + " ]";
    }
    
}
