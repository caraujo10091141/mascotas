/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Araujo
 */
@Embeddable
public class OrdenmascotaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "orden_id")
    private int ordenId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mascota_id")
    private int mascotaId;

    public OrdenmascotaPK() {
    }

    public OrdenmascotaPK(int ordenId, int mascotaId) {
        this.ordenId = ordenId;
        this.mascotaId = mascotaId;
    }

    public int getOrdenId() {
        return ordenId;
    }

    public void setOrdenId(int ordenId) {
        this.ordenId = ordenId;
    }

    public int getMascotaId() {
        return mascotaId;
    }

    public void setMascotaId(int mascotaId) {
        this.mascotaId = mascotaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ordenId;
        hash += (int) mascotaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrdenmascotaPK)) {
            return false;
        }
        OrdenmascotaPK other = (OrdenmascotaPK) object;
        if (this.ordenId != other.ordenId) {
            return false;
        }
        if (this.mascotaId != other.mascotaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.OrdenmascotaPK[ ordenId=" + ordenId + ", mascotaId=" + mascotaId + " ]";
    }
    
}
