/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import carro.Carro;
import carro.CarroItem;
import entidad.Cliente;
import entidad.Mascota;
import entidad.Orden;
import entidad.Ordenmascota;
import entidad.OrdenmascotaPK;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Evelyn Anaya
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class OrdenAdmin {

    @PersistenceContext(unitName = "ProyectoMascotaPU")
    private EntityManager em;
    @Resource
    private SessionContext context;
    @EJB
    private MascotaFacade productFacade;
    @EJB
    private OrdenFacade clienteOrderFacade;
    @EJB
    private OrdenmascotaFacade ordenMascotaFacade;

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int colocarOrden(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero, Carro carro) {

        try {
            Cliente cliente = addCliente(nombre, email, telefono, direccion, cp, tarjNumero);
            Orden orden = addOrder(cliente, carro);
            addOrderedItems(orden, carro);
            return orden.getId();
        } catch (Exception e) {
            e.printStackTrace();
            context.setRollbackOnly();
            return 0;
        }
    }

    private Cliente addCliente(String nombre, String email, String telefono, String direccion, String cp, String tarjNumero) {
        //Inicializa un nuevo cliente
        Cliente customer = new Cliente();

        //Se agregan los datos del cliente a los atributos
        customer.setNombre(nombre);
        customer.setEmail(email);
        customer.setTelefono(telefono);
        customer.setDireccion(direccion);
        customer.setCp(cp);
        customer.setTarjNumero(tarjNumero);

        em.persist(customer);
        return customer;
    }

    private Orden addOrder(Cliente cliente, Carro carro) {

        //Inicializa una nueva Orden
        Orden orden = new Orden();
        
        //Pone el id del cliente
        orden.setClienteId(cliente);
        //Obtiene el total de la compra + cargoEnvio y lo pone en el total para la BD
        orden.setTotal(BigDecimal.valueOf(carro.getTotal()));

        java.util.Date dt = new java.util.Date();

        //Genera un numero aleatorio para el numConfirmacion
        Random random = new Random();
        int i = random.nextInt(999999999);
        orden.setFechaCreacion(dt);
        orden.setNumConfirmacion(i);

        em.persist(orden);
        return orden;
    }

    //Agrega todas las mascotas del carrito a Ordenmascota
    private void addOrderedItems(Orden orden, Carro carro) {
        
        em.flush();

        List<CarroItem> items = carro.getItems();

        //Ciclo para meter todos los productos que contenga el carrito a la base de datos
        for (CarroItem scItem : items) {

            int productId = scItem.getMascota().getId();

            //Crea la llave foranea compuesta de los campos idMascota y idOrden
            OrdenmascotaPK orderedMascotaPK = new OrdenmascotaPK();
            orderedMascotaPK.setOrdenId(orden.getId());
            orderedMascotaPK.setMascotaId(productId);

            //Crea Ordenmascota a partir de la llave compuesta
            Ordenmascota ordenItem = new Ordenmascota(orderedMascotaPK);

            //Asigna la cantidad pedida
            ordenItem.setCantidad(scItem.getCantidad());

            em.persist(ordenItem);
        }
    }

    public Map getDetallesOrden(int ordenId) {

        Map orderMap = new HashMap();

        // Obtiene la orden
        Orden order = clienteOrderFacade.find(ordenId);

        // Obtiene el cliente
        Cliente customer = order.getClienteId();

        // Obtiene las mascotas del pedido buscandolas por ID
        List<Ordenmascota> Mascotas = ordenMascotaFacade.findByOrdenId(ordenId);
        
        //Lista de mascotas pedidas
        List<Mascota> mascotas = new ArrayList<>();

        for (Ordenmascota op : Mascotas) {

            Mascota p = (Mascota) productFacade.find(op.getOrdenmascotaPK().getMascotaId());
            mascotas.add(p);
        }

        //Obtiene los cuatro parametros que utlizaramos en confirmacion en un mapeo
        //idOrden
        //Cliente
        //Mascotas pedidas
        orderMap.put("orderRecord", order);
        orderMap.put("customer", customer);
        orderMap.put("orderedProducts", Mascotas);
        orderMap.put("products", mascotas);

        return orderMap;
    }

}
