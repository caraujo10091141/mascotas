/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.Ordenmascota;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Araujo
 */
@Stateless
public class OrdenmascotaFacade extends AbstractFacade<Ordenmascota> {
    @PersistenceContext(unitName = "ProyectoMascotaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public OrdenmascotaFacade() {
        super(Ordenmascota.class);
    }
    public List<Ordenmascota> findByOrdenId(Object ordenId) {
    
        return em.createNamedQuery("Ordenmascota.findByOrdenId").setParameter("ordenId", ordenId).getResultList();
    
    }
    
    
}
