
package control;

import carro.Carro;
import entidad.Categoria;
import entidad.Mascota;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sesion.CategoriaFacade;
import sesion.MascotaFacade;
import sesion.OrdenAdmin;

/**
 *
 * @author Luis Terrazas
 */
@WebServlet(name = "Controller",
            loadOnStartup = 1,
            urlPatterns = {"/categoria",
                           "/addTocarro",
                           "/viewcarro",
                           "/updatecarro",
                           "/pago",
                           "/hacerpago",
                           "/chooseLanguage"})

public class ControlServlet extends HttpServlet {

    private String cargo;

    @EJB
    private CategoriaFacade categoriaFacade;
    @EJB
    private MascotaFacade mascotaFacade;
    @EJB
    private OrdenAdmin OrdenAdmin;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {

        super.init(servletConfig);

        //Obtiene el cargo de envio se define en web.xml
        cargo = servletConfig.getServletContext().getInitParameter("cargoDeEnvio");

        getServletContext().setAttribute("categorias", categoriaFacade.findAll());
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Categoria categoSel;
        Collection<Mascota> mascotas;
        
        //Comprueba que el usuario haya seleccionado alguna categoria
        if (userPath.equals("/categoria")) {

            String categoriaId = request.getQueryString();

            if (categoriaId != null) {
                
                //Busca la categoria en la BD por id
                categoSel = categoriaFacade.find(Short.parseShort(categoriaId));
                
                //Inicializa un atributo de sesion con la categoria seleccionada
                session.setAttribute("categoriaSeleccionada", categoSel);

                //Obtiene las mascotas de la categoria seleccionada
                mascotas = categoSel.getMascotaCollection();
                
                //Inicializa el parametro categoriaMascotas con la coleccion de mascotas
                session.setAttribute("categoriaMascotas", mascotas);
            }
         
            //Si el usuario desea ver el contenido del carrito
         } else if (userPath.equals("/viewcarro")) {

            String clear = request.getParameter("clear");

            //Comprueba que el carrito contenga mascotas
            if ((clear != null) && clear.equals("true")) {

                Carro carro = (Carro) session.getAttribute("carro");
                carro.clear();
            }

            userPath = "/carro";

          //Si el usuario desea realizar el pago de su compra
        } else if (userPath.equals("/pago")) {

            Carro carro = (Carro) session.getAttribute("carro");

            carro.calculateTotal(cargo);


        } 


        String url = "/WEB-INF/vista" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");  

        String userPath = request.getServletPath();
        HttpSession session = request.getSession();
        Carro carro = (Carro) session.getAttribute("carro");
        

        //el usuario desea añadir productos al carrito
        if (userPath.equals("/addTocarro")) {

           //Si no se ha inicializado el atributo carro, 
            if (carro == null) {

                carro = new Carro();
                session.setAttribute("carro", carro);
            }
            //Obtiene el id de la mascota
            String productId = request.getParameter("productId");

            //Si existe la mascota
            if (!productId.isEmpty()) {
                
                //Encuentra la mascota por id
                Mascota mascota = mascotaFacade.find(Integer.parseInt(productId));
                
                //Agrega la mascota al carrito
                carro.agregaItem(mascota);
            }

            userPath = "/carro";


        } else if (userPath.equals("/updatecarro")) {

          //Obtiene el id de la mascota
           String mascotaId = request.getParameter("productId");
           
           //Obtiene la cantidad a actualizar
            String cantidad = request.getParameter("quantity");


               if(true){
                //Encuentra la mascota por id
                Mascota mascota = mascotaFacade.find(Integer.parseInt(mascotaId));
                
                //actualiza la cantidad en la mascota elegida
                carro.actualiza(mascota, cantidad);
            }

            userPath = "/carro";


        } else if (userPath.equals("/hacerpago")) {

            if (carro != null) {
                
                //Obtiene los datos ingresados en el formulario
                String nombre = request.getParameter("name");
                String email = request.getParameter("email");
                String tel = request.getParameter("phone");
                String dir = request.getParameter("address");
                String cp = request.getParameter("cityRegion");
                String tjNum = request.getParameter("creditcard");

                boolean error = false; 
                error = false;

                if (error == true) {
                      
                    request.setAttribute("validationErrorFlag", error);
                    userPath = "/pago";

                } else {
                     
                    //Pone la orden en el Administrador
                    int orderId = OrdenAdmin.colocarOrden(nombre, email, tel, dir, cp, tjNum, carro);
                    
                    //Comprueba que la orden haya sido exitosa
                    if (orderId != 0) {
                        
                        //Vacia el carrito
                        carro = null;
                        
                        //Invalida o cierra la session
                        session.invalidate();
                        
                        //Obtiene un mapeo de los datos y los inicializa para ser usados en confirmacion
                        Map orderMap = OrdenAdmin.getDetallesOrden(orderId);
                        request.setAttribute("customer", orderMap.get("customer"));
                        request.setAttribute("products", orderMap.get("products"));
                        request.setAttribute("orderRecord", orderMap.get("orderRecord"));
                        request.setAttribute("orderedProducts", orderMap.get("orderedProducts"));
                        
                        userPath = "/confirmacion";

                    } else {
                        userPath = "/pago";
                        
                        request.setAttribute("orderFailureFlag", true);
                    }
                }
            }
        }

        String url = "/WEB-INF/vista" + userPath + ".jsp";

        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}