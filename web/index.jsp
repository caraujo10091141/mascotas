<p style="font-size: larger">Bienvenido a Nuesta Tienda de Mascotas</p>

<div id="indexLeftColumn">
    <div id="welcomeText">
        <p>En nuestra tienda de mascotas NAGO puedes encontrar una gran cantidad de animales para tu hogar.
        Selecciona una de las categoriasque estan a la derecha para empezar a comprar.
        </p>
    </div>
</div>

<div id="indexRightColumn">
    <table align="center"> 
<tr> 
    <td align="center"> 
    Categorias
    </td>
</tr>
</table>
     <table>
      <tr>
      <c:forEach var="categoria" items="${categorias}">
        <div class="categoriaBox">
            <td><a href="<c:url value='categoria?${categoria.id}'/>">
                <span class="categoriaLabel"></span>
                <span class="categoriaLabelText">${categoria.nombre}</span>
                <img src="${initParam.categoriasImg}${categoria.nombreImg}.jpg"
                         alt="${categoria.nombreImg}" class="categoriaImage">
                
                    </a>
            </td>
        </div>
    </c:forEach>
           </tr>
  </table>                    
</div>