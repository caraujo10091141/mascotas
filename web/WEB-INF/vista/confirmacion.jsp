<%--
    Document   : Confirmacion
    Created on : Nov 30, 2014, 17:20:43 PM
    Author     : Cesar Araujo
--%>


<div id="singleColumn">

    <p id="confirmacionText">
        <strong>tu orden ha sido procedada correctamente y tu envio sera realizado en no menos de 24 horas.</strong>
        <br><br>
        Guarda el Numero de Confirmacion:
        <strong>${orderRecord.numConfirmacion}</strong>
        <br>
        Si tienes alguna duda contactanos gratis <a href="#">Contacto</a>.
        <br><br>
        Gracias por tu compra.
    </p>

    <div class="summaryColumn" >

        <table id="orderSummaryTable" class="detailsTable">
            <tr class="header">
                <th colspan="3">Detalles de la Orden</th>
            </tr>

            <tr class="lightBlue"  >
                <td>Mascota</td>
                <td>Cantidad</td>
                <td>Precio</td>
            </tr>

            <c:forEach var="orderedProduct" items="${orderedProducts}" varStatus="iter">

                <tr class="${((iter.index % 2) != 0) ? 'lightBlue' : 'white'}">
                    <td>${products[iter.index].nombre}</td>
                    <td class="quantityColumn">
                        ${orderedProduct.cantidad}
                    </td>
                    <td class="confirmacionPriceColumn">
                        $ ${products[iter.index].precio * orderedProduct.cantidad}
                    </td>
                </tr>

            </c:forEach>

            <tr class="lightBlue"><td colspan="3" style="padding: 0 20px"><hr></td></tr>

            <tr class="lightBlue">
                <td colspan="2" id="cargoDeEnvioCellLeft"><strong>Cargo de Envio</strong></td>
                <td id="cargoDeEnvioCellRight">$ ${initParam.cargoDeEnvio}</td>
            </tr>

            <tr class="lightBlue">
                <td colspan="2" id="totalCellLeft"><strong>Total:</strong></td>
                <td id="totalCellRight">$ ${orderRecord.total}</td>
            </tr>

            <tr class="lightBlue"><td colspan="3" style="padding: 0 20px"><hr></td></tr>

            <tr class="lightBlue">
                <td colspan="3" id="dateProcessedRow"><strong>Fecha</strong>
                    ${orderRecord.fechaCreacion}
                </td>
            </tr>
        </table>

    </div>

    <div class="summaryColumn" >

        <table id="deliveryAddressTable" class="detailsTable">
            <tr class="header">
                <th colspan="3">Direccion para el Envio</th>
            </tr>

            <tr>
                <td colspan="3" class="lightBlue">
                    A nombre de: ${customer.nombre}
                    <br>
                    Direccion: ${customer.direccion}
                    <br>
                    Codigo Postal: ${customer.cp}
                    <br>
                    <hr>
                    <strong>Email:</strong> ${customer.email}
                    <br>
                    <strong>Telefono:</strong> ${customer.telefono}
                </td>
            </tr>
        </table>

    </div>
</div>