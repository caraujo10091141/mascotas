<%--
    Document   : categoria
    Created on : Nov 28, 2014, 12:20:12 PM
    Author     : Evelyn Anaya
--%>


<div id="categoriaLeftColumn">

    <c:forEach var="categoria" items="${categorias}">

        <c:choose>
            <c:when test="${categoria.nombre == categoriaSeleccionada.nombre}">
                <div class="categoriaButton" id="selectedcategoria">
                    <span class="categoriaText">
                        ${categoria.nombre}
                    </span>
                </div>
            </c:when>
            <c:otherwise>
                <a href="<c:url value='categoria?${categoria.id}'/>" class="categoriaButton">
                    <span class="categoriaText">
                        ${categoria.nombre}
                    </span>
                </a>
            </c:otherwise>
        </c:choose>

    </c:forEach>

</div>

<div id="categoriaRightColumn">

    <p id="categoriaTitle"><fmt:message key="${categoriaSeleccionada.nombre}" /></p>

    <table id="productTable" align="center">

        <c:forEach var="product" items="${categoriaMascotas}" varStatus="iter">

            <tr class="${((iter.index % 2) == 0) ? 'lightBlue' : 'white'}">
                <td>
                    <img src="${initParam.MascotasImg}${product.nombreImg}.jpg"
                         alt="${product.nombreImg}">
                </td>

                <td>
                    <br>
                    <span class="smallText">${product.nombre}</span>
                </td>

                <td>$ ${product.precio}</td>

                <td>
                    <form action="<c:url value='addTocarro'/>" method="post">
                        <input type="hidden"
                               name="productId"
                               value="${product.id}">
                        <input type="submit"
                               name="submit"
                               value="A�adir al Carrito">
                    </form>
                </td>
            </tr>

        </c:forEach>

    </table>
</div>