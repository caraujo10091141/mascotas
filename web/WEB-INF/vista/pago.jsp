<%--
    Document   : Formato para hacer el Pago
    Created on : Nov 30, 2014, 10:20:30 AM
    Author     : Evelyn Anaya
--%>



<div id="singleColumn">

    <h2>Realizar Pago</h2>

    <p>A continuacion se hara el pago de los productos en su carrito, porfavor proporcionenos la informacion que le pedimos:</p>

<script type="text/javascript">
		  
		  
		  function esVacio(cadena){
			
			    var r=false;
				
				if(cadena.length==0 || cadena.charAt(0)==' '){
				  r=true;
				}
				
		    return r;
		}
		
		function esLetra(c){
                    
                    if(c){
                        
                        
                    }
                }
		
		function esDigito(c){
		  if((c>="0") && (c<="9")){
		    return true;
		  }
		  else{
		    return false;
		  }
		}
		
		function esNumerico(numero){
		  var r=true;
		  
		  for(var i=0; i<numero.length; i++){
		    if(esDigito(numero.charAt(i))==false){
			  r=false;
			}
		  }
		  return r;
		}
		
		function esEmail(s){
				var i=1;
				var sLength=s.length;
				//alert(s.charAt(i));
				while((i<sLength)&&(s.charAt(i)!="@")){
					i++;
					//alert(i);
				}
				if((i>=sLength)||(s.charAt(i)!="@")){
					return false;
				}else{i+=2;}
				while((i<sLength)&&(s.charAt(i)!=".")){
					i++;
				}
				if((i>=(sLength-1))||(s.charAt(i)!=".")) {
					//alert(s.charAt(i-1));
					alert("Error");
					return false;
				}else{ return true;}
			}	
			
		
		function Validar(formulario){
			if((esVacio(formulario.name.value)==true)){
			  formulario.name.focus();
			  alert("Debe ingersar su nombre");
			  return false;
			}
			else{
			
			  if((esVacio(formulario.email.value)==true)){
			    formulario.email.focus();
			    alert("Debe ingresar su correo electronico");
				return false;
                            }
                                if((esEmail(formulario.email.value)==false)){
                                formulario.email.focus();
                                alert("Ingrese un email valido");
				return false;
			  
			  }
			  else{
			    if((esVacio(formulario.phone.value)==true)){
			      formulario.phone.focus();
			      alert("Debe ingresar su numero de telefono");
				  return false;
			    }
                               
                                if((esNumerico(formulario.phone.value)==false)){
                                formulario.phone.focus();
			        alert("Ingrese correctamente su telefono, solo acepta datos numericos");
				return false;
                              }
                           
                                if(formulario.phone.value.length!=10){                                
                               formulario.phone.focus();
			      alert("Su telefono debe contener 10 digitos");
				  return false;
                              }
                                
				else{
				  if((esVacio(formulario.address.value)==true)){
			        formulario.address.focus();
			        alert("Debe ingresar su domicilio"); 
					return false;
		                    }
                                    
				  else{
                                       if((esVacio(formulario.cityRegion.value)==true)){
                                       formulario.cityRegion.focus();
                                       alert("Debe ingresar su numero codigo postal");
				       return false;
                                        }
                                            if((esNumerico(formulario.cityRegion.value)==false)){
                                            formulario.cityRegion.focus();
				            alert("Su Codigo Postal es numerico"); 
                                            return false;
					  }
                                                         if(formulario.cityRegion.value.length!=5){                                
                                                         formulario.cityRegion.focus();
                                                        alert("Su codigo postal debe contener 5 digitos");
                                                         return false;
                                                        }
				    
					
					  else{
                                              
                                              if((esVacio(formulario.creditcard.value)==true)){
                                              formulario.cityRegion.focus();
                                              alert("Debe ingresar su numero de tarjeta");
				              return false;
                                                 }
                                                 
                                                      if((esNumerico(formulario.creditcard.value)==false)){
                                                     formulario.creditcard.focus();
                                                      alert("Ingrese correctamente el numero de su tarjeta, solo acepta datos numericos");
                                                      return false;
                                                      }
                           
                                                         if(formulario.creditcard.value.length!=16){                                
                                                         formulario.creditcard.focus();
                                                        alert("Su numero de tarjeta debe contener 16 digitos");
                                                         return false;
                                                        }
					    
						
								  else{
								    return true;
								  }
                           		    }
				        }
				    }
				}
			    }
                        }
                   
            
		</script>

                <form id="formulario" name="formulario" action="<c:url value='hacerpago'/>" method="post" onsubmit="return Validar(this);">
        <table id="pagoTable">
            <tr>
                <td><label for="name">Nombre Completo:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="45"
                           id="name"
                           name="name"
                           value="${param.name}"
                           >
                </td>
            </tr>
            <tr>
                <td><label for="email">Email:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="45"
                           id="email"
                           name="email"
                           value="${param.email}"
                           >
                    
                </td>
            </tr>
            <tr>
                <td><label for="phone">Telefono:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="10"
                           id="phone"
                           name="phone"
                           value="${param.phone}"
                        >
                </td>
            </tr>
            <tr>
                <td><label for="address">Direccion:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="45"
                           id="address"
                           name="address"
                           value="${param.address}"
                           >
                </td>
            </tr>
                    <tr>
                    <td><label for="address">Codigo Postal:</label></td>
                    <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="45"
                           id="cityRegion"
                           name="cityRegion"
                           value="${param.cityRegion}"
                         >
                </td>
            </tr>
            <tr>
                <td><label for="creditcard">Numero de Tarjeta de Credito:</label></td>
                <td class="inputField">
                    <input type="text"
                           size="31"
                           maxlength="16"
                           id="creditcard"
                           name="creditcard"
                           value="${param.creditcard}"
                        >
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" value="Realizar Pago" >
                </td>
            </tr>
        </table>
    </form>

    <div id="infoBox">

        <ul>
            <li>Garantia de Entrega al siguiente dia</li>
            <li>Un $ ${initParam.cargoDeEnvio}
                de cargo al hacer el envio</li>
        </ul>

        <table id="priceBox">
            <tr>
                <td>Subtotal:</td>
                <td class="pagoPriceColumn">
                    $ ${carro.subtotal}</td>
            </tr>
            <tr>
                <td>Cargo de Envio</td>
                <td class="pagoPriceColumn">
                    $ ${initParam.cargoDeEnvio}</td>
            </tr>
            <tr>
                <td class="total">Total:</td>
                <td class="total pagoPriceColumn">
                    $ ${carro.total}</td>
            </tr>
        </table>
    </div>
</div>