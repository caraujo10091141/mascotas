<%--
    Document   : carro
    Created on : Nov 29, 2014, 12:20:12 AM
    Author     : Cesar Araujo
--%>


<div id="singleColumn">

    <c:choose>
        <c:when test="${carro.numeroDeItems > 1}">
            <p>Tu carrito contiene${carro.numeroDeItems} items.</p>
        </c:when>
        <c:when test="${carro.numeroDeItems == 1}">
            <p>Tu carrito contiene ${carro.numeroDeItems} item.</p>
        </c:when>
        <c:otherwise>
            <p>Tu carrito esta vacio.</p>
        </c:otherwise>
    </c:choose>

    <div id="actionBar">
        <%-- clear carro widget --%>
        <c:if test="${!empty carro && carro.numeroDeItems != 0}">

            <c:url var="url" value="viewcarro">
                <c:param name="clear" value="true"/>
            </c:url>

            <a href="${url}" class="bubble hMargin">Limpiar Carrito</a>
        </c:if>

        <%-- continue shopping widget --%>
        <c:set var="value">
            <c:choose>
                <%-- if 'selectedcategoria' session object exists, send user to previously viewed categoria --%>
                <c:when test="${!empty categoriaSeleccionada}">
                    categoria
                </c:when>
                <%-- otherwise send user to welcome page --%>
                <c:otherwise>
                    index.jsp
                </c:otherwise>
            </c:choose>
        </c:set>

        <c:url var="url" value="${value}"/>
        <a href="${url}" class="bubble hMargin">Continuar Comprando</a>

        <%-- pago widget --%>
        <c:if test="${!empty carro && carro.numeroDeItems != 0}">
            <a href="<c:url value='pago'/>" class="bubble hMargin">Proceder al pago&#x279f;</a>
        </c:if>
    </div>

    <c:if test="${!empty carro && carro.numeroDeItems != 0}">

      <h4 id="subtotal">Subtotal: $ ${carro.subtotal}</h4>

      <table id="carroTable" align="center">

        <tr class="header">
            <th>Producto</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Cantidad</th>
        </tr>

        <c:forEach var="carroItem" items="${carro.items}" varStatus="iter">

          <c:set var="product" value="${carroItem.mascota}"/>

          <tr class="${((iter.index % 2) == 0) ? 'lightBlue' : 'white'}">
            <td>
              <img src="${initParam.MascotasImg}${product.nombreImg}.jpg"
                   alt="${product.nombre}">
            </td>

            <td>${product.nombre}</td>

            <td>
                $ ${carroItem.total}
                <br>
                <span class="smallText">( $ ${product.precio} / unidad )</span>
            </td>

            <td>
                <form action="<c:url value='updatecarro'/>" method="post">
                    <input type="hidden"
                           name="idMascota"
                           value="${product.id}">
                    <input type="text"
                           maxlength="2"
                           size="2"
                           value="${carroItem.cantidad}"
                           name="cantidad"
                           style="margin:5px">
                    <input type="submit"
                           name="submit"
                           value="Actualizar">
                </form>
            </td>
          </tr>

        </c:forEach>

      </table>

    </c:if>
</div>